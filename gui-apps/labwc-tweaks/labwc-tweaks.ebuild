# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="tweak tool for labwc"
HOMEPAGE="https://labwc.github.io/"
SRC_URI="https://github.com/labwc/labwc-tweaks/archive/refs/heads/master.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
