# 10leej-overlay
This is my custom gentoo repository where I'm adding packages. These are for me, and maybe you. Mostly just supplementing stuff here.

## Enable

to enable this repository you can use [eselect-repositoy](https://wiki.gentoo.org/wiki/Eselect/Repository) like this.

`` # eselect repository add 10leej-overlay git https://gitlab.com/10leej/10leej-overlay ``

then like every other 3rd party overlay I use git for this repo, so you need to had [Git](https://wiki.gentoo.org/wiki/Git) installed on your gentoo system

`` # emerge --sync 10leej-overlay ``
